import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CardModule } from 'primeng/card';

import { WeatherComponent } from './weather.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainNavModule } from '../main-nav/main-nav.module';

@NgModule({
  declarations: [WeatherComponent],
  imports: [
    CommonModule,
    FormsModule,
    CardModule,
    BrowserAnimationsModule,
    MainNavModule
  ],
  exports: [WeatherComponent]
})
export class WeatherModule { }
